console.log('\nNo. 1');
console.log('=====');

function arrayToObject(input) {
    if(input != null || input == []) {
        var now = new Date()
        var thisYear = now.getFullYear() // 2020 (tahun sekarang)

        for(var i = 0; i < input.length; i++) {
            if(input[i][3] != null) {
                if(input[i][3] < thisYear) {
                    var data = {
                        [i+1 +". " + input[i][0] + " " + input[i][1]] : {
                            firstName : input[i][0],
                            lastName : input[0][1],
                            gender : input[i][2],
                            age : thisYear - input[i][3]
                        }
                    }
                } else {
                    var data = {
                        [i+1 +". " + input[i][0] + " " + input[i][1]] : {
                            firstName : input[i][0],
                            lastName : input[0][1],
                            gender : input[i][2],
                            age : "Invalid birth year"
                        }
                    }
                }
            } else {
                var data = {
                    [i+1 +". " + input[i][0] + " " + input[i][1]] : {
                        firstName : input[i][0],
                        lastName : input[0][1],
                        gender : input[i][2],
                        age : "Invalid birth year"
                    }
                }
            }

            console.log(data);
        }
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people);
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2);

console.log('\nNo. 2');
console.log('=====');

function shoppingTime(memberId, money) {
    var barang =[
        {
            jenis: 'Sepatu',
            namaBarang: 'Stacattu',
            harga: 1500000
        },
        {
            jenis: 'Baju',
            namaBarang: 'Zoro',
            harga: 500000
        },
        {
            jenis: 'Baju',
            namaBarang: 'H&N',
            harga: 250000
        },
        {
            jenis: 'Sweater',
            namaBarang: 'Uniklooh',
            harga: 175000
        },
        {
            jenis: 'Casing',
            namaBarang: 'Handphone',
            harga: 50000
        }
    ]
    if(memberId != '' && money >= 50000) {
        var output = {}
        var daftarBelanja = [];

        output.memberId = memberId;
        output.money = money;
        for(var i = 0; i < barang.length; i++) {
            if(money >= barang[i].harga) {
                daftarBelanja.push(barang[i].jenis + " " + barang[i].namaBarang);
                money -= barang[i].harga
            }
            output.listPurchased = daftarBelanja
            output.changeMoney = money
        }

        return output;
    } else if(memberId == null || memberId == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if(money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\nNo. 3');
console.log('=====');

function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here

    var outputArrObject = [];
    for(var i = 0; i < arrPenumpang.length; i++) {
        var output = {};
        output.penumpang = arrPenumpang[i][0]
        output.naikDari = arrPenumpang[i][1]
        output.tujuan = arrPenumpang[i][2]

        var awal = rute.indexOf(arrPenumpang[i][1]);
        var tujuan = rute.indexOf(arrPenumpang[i][2]);
        var ongkos = (tujuan - awal) * 2000;

        output.bayar = ongkos;

        outputArrObject.push(output)
    }

    return outputArrObject;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));