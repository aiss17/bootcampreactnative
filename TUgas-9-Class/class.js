console.log('\nNo. 1');
console.log('=====');

class Animal {
    // Code class di sini

    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }

}

class Ape extends Animal {
    constructor(name) {
      super(name);
      this.legs = 2;
    }
    yell() {
      return console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(brand, mod) {
      super(brand);
      this.model = mod;
    }
    jump() {
      return console.log("hop hop");
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('\nNo. 2');
console.log('=====');

// function Clock({ template }) {
  
//     var timer;
  
//     function render() {
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
  
//       console.log(output);
//     }
  
//     this.stop = function() {
//       clearInterval(timer);
//     };
  
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
  
// }

class Clock {
    // Code di sini
    constructor({template}) {
        this.date
        this.hours
        this.mins
        this.secs
        this.timer
        this.output
        this.template = template

    }

    render() {
        this.date = new Date();
        this.hours = this.date.getHours()
        if (this.hours < 10) {
            this.hours = '0' + this.hours;
        }
        this.mins = this.date.getMinutes()
        if (this.mins < 10) {
            this.mins = '0' + this.mins;
        }
        this.secs = this.date.getSeconds()
        if (this.secs < 10) {
            this.secs = '0' + this.secs;
        }
        this.output = this.template
        .replace('h', this.hours)
        .replace('m', this.mins)
        .replace('s', this.secs);

        console.log(this.output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render
        this.timer = setInterval(this.render.bind(this), 1000)
    }
}

var clock = new Clock({ template: 'h:m:s' });
clock.start();