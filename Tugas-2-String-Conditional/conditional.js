var nama = 'John';
var peran = 'warewolf';

console.log('No. 1');
console.log('=====');
if(nama == '') {
    console.log('Nama harus diisi!\n');
} else if(peran == '') {
    console.log('Pilih Peranmu untuk memulai game\n');
} else if(nama != '' && peran == '') {
    console.log('Halo ' + nama + ', ' + 'Pilih peranmu untuk memulai game!\n');
} else if(nama != '' && peran.toLocaleLowerCase() == 'penyhir') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama);
    console.log('Halo Penyihir ' + nama + ', ' + 'kamu dapat melihat siapa yang menjadi werewolf!\n');
} else if(nama != '' && peran.toLocaleLowerCase() == 'guard') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama);
    console.log('Halo Guard ' + nama + ', ' + 'kamu akan membantu melindungi temanmu dari serangan werewolf.\n');
} else if(nama != '' && peran.toLocaleLowerCase() == 'warewolf') {
    console.log('Selamat datang di Dunia Werewolf, ' + nama);
    console.log('Halo Werewolf ' + nama + ', ' + 'Kamu akan memakan mangsa setiap malam!\n');
}

var hari = 21; 
var bulan = 3; 
var tahun = 1945;

switch(bulan){
    case 1: bulan = 'Januari'; break;
    case 2: bulan = 'Februari'; break;
    case 3: bulan = 'Maret'; break;
    case 4: bulan = 'April'; break;
    case 5: bulan = 'Mei'; break;
    case 6: bulan = 'Juni'; break;
    case 7: bulan = 'Juli'; break;
    case 8: bulan = 'Agustus'; break;
    case 9: bulan = 'September'; break;
    case 10: bulan = 'Oktober'; break;
    case 11: bulan = 'November'; break;
    case 12: bulan = 'Desember'; 
}
console.log('No. 2');
console.log('=====');
console.log(hari + ' ' + bulan + ' ' + tahun);