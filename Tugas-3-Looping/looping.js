var loop = 0;
var nilai = 0;

console.log('No. 1');
console.log('=====\n');
console.log('LOOPING PERTAMA');
while(loop < 20) {
    if(loop < 10) {
        nilai += 2;
        console.log(nilai + ' - ' + 'I Love Coding');
    } else {
        if(nilai == 20) {
            console.log('LOOPING KEDUA');
        }
        console.log(nilai + ' - ' + 'I will become a mobile developer');
        nilai -= 2;
    }
    loop++;
}

console.log('\nNo. 2');
console.log('=====\n');

var banyakLooping = 20;

for(var i = 1; i <= banyakLooping ; i++) {
    if(i % 2 != 0 && i % 3 != 0) {
        console.log(i + ' - ' + 'Santai');
    } else if(i % 2 == 0) {
        console.log(i + ' - ' + 'Berkualitas');
    } else if(i % 2 != 0 && i % 3 == 0) {
        console.log(i + ' - ' + 'I Love Coding.');
    }
}

console.log('\nNo. 3');
console.log('=====\n');

var hasil = '';
for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 8; j++) {
        hasil += '#';
    }
    hasil += '\n';
}
console.log(hasil);

console.log('\nNo. 4');
console.log('=====\n');

var hasil = '';
for (var i = 0; i < 8; i++) {
    for (var j = 0; j < i; j++) {
        hasil += '#';
    }
    hasil += '\n';
}
console.log(hasil);

console.log('\nNo. 5');
console.log('=====\n');

var hasil = '';
for (var i = 0; i < 8; i++) {
    for (var j = 0; j < 8; j++) {
        if(i % 2 == 0 && j % 2 !== 0) {
            hasil += '#';
        } else if(i % 2 == 0 && j % 2 == 0) {
            hasil += ' ';
        } else if(i % 2 != 0 && j % 2 !== 0) {
            hasil += ' ';
        } else if(i % 2 != 0 && j % 2 == 0) {
            hasil += '#';
        }
    }
    hasil += '\n';
}
console.log(hasil);