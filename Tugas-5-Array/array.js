console.log('\nNo. 1');
console.log('=====');

function range(startNum, finishNum) {
    var urut = [];
    if(startNum != null && finishNum != null) {
        if(startNum < finishNum) {
            for(startNum; startNum <= finishNum; startNum++) {
                urut.push(startNum);
            }
            return urut;
        } else {
            for(startNum; startNum >= finishNum; startNum--) {
                urut.push(startNum);
            }
            return urut;
        }
    } else {
        return -1;
    }
}

console.log(range(1));

console.log('\nNo. 2');
console.log('=====');

function rangeWithStep(startNum, finishNum, step) {
    var urut = [];
    if(startNum != null && finishNum != null) {
        if(startNum < finishNum) {
            for(startNum; startNum <= finishNum; startNum+=step) {
                urut.push(startNum);
            }
            return urut;
        } else {
            for(startNum; startNum >= finishNum; startNum-=step) {
                urut.push(startNum);
            }
            return urut;
        }
    } else {
        return -1;
    }
}

console.log(rangeWithStep(10, 1, 2));

console.log('\nNo. 3');
console.log('=====');

function sum(startNum, finishNum, step) {
    var total = 0;
    if(startNum != null && finishNum != null) {
        if(startNum < finishNum) {
            if(step != null) {
                for(startNum; startNum <= finishNum; startNum+=step) {
                    total += startNum;
                }
                return total;
            } else {
                for(startNum; startNum <= finishNum; startNum++) {
                    total += startNum;
                }
                return total;
            }
        } else {
            if(step != null) {
                for(startNum; startNum >= finishNum; startNum-=step) {
                    total += startNum;
                }
                return total;
            } else {
                for(startNum; startNum >= finishNum; startNum--) {
                    total += startNum;
                }
                return total;
            }
        }
    } else {
        return -1;
    }
}

console.log(sum(1,10));

console.log('\nNo. 4');
console.log('=====');

function dataHanding(data) {
    for(var i = 0; i < data.length; i++){
        var nomorID = "Nomor ID: " + data[i][0];
        var namaLengkap = "Nama Lengkap: " + data[i][1];
        var tempatTanggalLahir = "TTL: " + data[i][2] + " " + data[i][3];
        var hobby = "Hobi: " + data[i][4] + "\n";

        console.log(nomorID);
        console.log(namaLengkap);
        console.log(tempatTanggalLahir);
        console.log(hobby)
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHanding(input);

console.log('\nNo. 5');
console.log('=====');

function balikKata(input){
    var inputLength = input.length - 1;
    var outputKata = '';
    for(inputLength; inputLength >= 0; inputLength--) {
        outputKata += input[inputLength];
    }

    return outputKata;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\nNo. 6');
console.log('=====');

function dataHandling2(input) {
    var nama = input [1] + " Elsharawy";
    var provinsi = "Provinsi " + input[2];
    var gender = "Pria";
    var sekolah = "SMA Internasional Metro";
    input.splice(1, 1, nama);
    input.splice(2, 1, provinsi);
    input.splice(4, 1, gender, sekolah);

    var tanggal = input[3];
    var splitTanggal = tanggal.split('/');
    var bulanPecah = splitTanggal[1];
    var namaBulan = '';

    switch(bulanPecah){
        case '01': namaBulan = 'Januari'; break;
        case '02': namaBulan = 'Februari'; break;
        case '03': namaBulan = 'Maret'; break;
        case '04': namaBulan = 'April'; break;
        case '05': namaBulan = 'Mei'; break;
        case '06': namaBulan = 'Juni'; break;
        case '07': namaBulan = 'Juli'; break;
        case '08': namaBulan = 'Agustus'; break;
        case '09': namaBulan = 'September'; break;
        case '10': namaBulan = 'Oktober'; break;
        case '11': namaBulan = 'November'; break;
        case '12': namaBulan = 'Desember';
        default: break;
    }

    var dateFormatBaru = splitTanggal.join("-");
    var arrDate = splitTanggal.sort(function(value1, value2){
        value2- value1
    })
    var editNama = nama.slice(0, 15);

    console.log(input);
    console.log(namaBulan);
    console.log(arrDate);
    console.log(dateFormatBaru);
    console.log(editNama);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);