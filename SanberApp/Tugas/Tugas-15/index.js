import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { SignIn, CreateAccount, Search, Home } from "./Screen";

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator()
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackNavigator = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} />
  </HomeStack.Navigator>
)

const SearchStackNavigator = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search} />
  </SearchStack.Navigator>
)

export default () => (
  <NavigationContainer>
    <Tabs.Navigator>
      <Tabs.Screen name="Home" component={HomeStackNavigator} />
      <Tabs.Screen name="Search" component={SearchStackNavigator} />
    </Tabs.Navigator>
    {/* <AuthStack.Navigator>
      <AuthStack.Screen 
        name="SignIn" 
        component={SignIn} 
        options={{ title: 'Sign In' }} 
      />
      <AuthStack.Screen 
        name="CreateAccount" 
        component={CreateAccount} 
        options={{ title: 'Create Account' }} 
      />
    </AuthStack.Navigator> */}
  </NavigationContainer>
)