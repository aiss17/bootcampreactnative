console.log('\nNo. 1');
console.log('=====');

function teriak() {
    return "Halo Sanbers!";
}
   
console.log(teriak());

console.log('\nNo. 2');
console.log('=====');

function kalikan(value1, value2) {
    return value1 * value2;
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

console.log('\nNo. 3');
console.log('=====');

function introduce(name, age, address, hobby) {
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!\n`;
}

var nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(nama, age, address, hobby)
console.log(perkenalan)